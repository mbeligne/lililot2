list.of.packages <- c("explor", "shiny","FactoMineR","tidyverse")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)>0) install.packages(new.packages)

library(explor)
library(FactoMineR)
library(tidyverse)

this.dir <- dirname(rstudioapi::getActiveDocumentContext()$path)
setwd(this.dir)

#ProfilByClasse_CAndE <- read_csv("R/lililot2/Result/Profil/ProfilByClasse_CAndE.csv", col_names = TRUE)
ProfilByClasse_CAndE <- read_csv("../Result/Profil/ProfilByClasse_CAndE.csv", col_names = TRUE)


# rename column 1
colnames(ProfilByClasse_CAndE)[1] <- "new_col"
#set specific column as row names
ProfilByClasse_CAndE_2 <- ProfilByClasse_CAndE %>% column_to_rownames(var="new_col")


ProfilByClasse_CAndE_2 <- subset(ProfilByClasse_CAndE_2, select = c('C1', 'C2','C3','C3BIS', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9', 'Poesie', 'Lycee'))

#Colonne a enlever sous la forme d'un vecteur
#pour ne rien enlever, col_names_df_to_remove<-c()
#pour enlever une colonne, par exemple C1, col_names_df_to_remove<-c('C1')
#pour enlever plusierurs colonnes, par exemple C7 et C8, col_names_df_to_remove<-c('C7','C8')
##############################################
col_names_df_to_remove<-c()  ####### C'est là pour enlever des colonnes
###############################################
ProfilByClasse_CAndE_2<-ProfilByClasse_CAndE_2[,!(names(ProfilByClasse_CAndE_2) %in% col_names_df_to_remove)]

# Possibilité d'enlever des lignes sur le même principe que les colonnes
row_names_df_to_remove<-c()
ProfilByClasse_CAndE_2<-ProfilByClasse_CAndE_2[!(row.names(ProfilByClasse_CAndE_2) %in% row_names_df_to_remove),]


# Recherche passée
#ProfilByClasse_CAndE_2[,13] <- rowSums(ProfilByClasse_CAndE_2) - ProfilByClasse_CAndE_2[,12] - ProfilByClasse_CAndE_2[,11]
#ProfilByClasse_CAndE_2[,14] <- c(183,464,393,356,44,182,32,175,79,130,179,144,74,30,47,102,159,150,243,401)
#ProfilByClasse_CAndE_2[,15] <- ProfilByClasse_CAndE_2[,13] - ProfilByClasse_CAndE_2[,14]

print(ProfilByClasse_CAndE_2)

BorneMinVariSup <- ncol(ProfilByClasse_CAndE_2)-1
BorneMaxVariSup <- ncol(ProfilByClasse_CAndE_2)
ca <- CA(ProfilByClasse_CAndE_2, col.sup=c(BorneMinVariSup: BorneMaxVariSup))

explor(ca)
