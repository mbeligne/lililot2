list.of.packages <- c("explor", "shiny","FactoMineR","tidyverse")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)>0) install.packages(new.packages)

library(explor)
library(FactoMineR)
library(tidyverse)

this.dir <- dirname(rstudioapi::getActiveDocumentContext()$path)
setwd(this.dir)

#ProfilByClasse_CAndE <- read_csv("R/lililot2/Result/Profil/ProfilByClasse_CAndE.csv", col_names = TRUE)
ProfilByClasse_CAndE <- read_csv("../Result/Profil/ProfilByClasse_CAndE.csv", col_names = TRUE)


# rename column 1
colnames(ProfilByClasse_CAndE)[1] <- "new_col"
#set specific column as row names
ProfilByClasse_CAndE_2 <- ProfilByClasse_CAndE %>% column_to_rownames(var="new_col")

print(ProfilByClasse_CAndE_2)

pca <- PCA(ProfilByClasse_CAndE_2,scale.unit=TRUE, quanti.sup=c(23: 24))
explor(pca)
