list.of.packages <- c("explor", "shiny","FactoMineR","tidyverse")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)>0) install.packages(new.packages)

library(explor)
library(FactoMineR)
library(tidyverse)

this.dir <- dirname(rstudioapi::getActiveDocumentContext()$path)
setwd(this.dir)

#ProfilByClasse_CAndE <- read_csv("R/lililot2/Result/Profil/ProfilByClasse_CAndE.csv", col_names = TRUE)
ProfilByEleve_CAndE <- read_csv("../Result/Profil/ProfilByEleve_CAndE.csv", col_names = TRUE)


# rename column 1
colnames(ProfilByEleve_CAndE)[1] <- "new_col"
#set specific column as row names
ProfilByEleve_CAndE_2 <- ProfilByEleve_CAndE %>% column_to_rownames(var="new_col")


ProfilByEleve_CAndE_2 <- subset(ProfilByEleve_CAndE_2, select = c('C1', 'C2','C3','C3BIS', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9', 'Poesie', 'Lycee'))

#Colonne a enlever sous la forme d'un vecteur
#pour ne rien enlever, col_names_df_to_remove<-c()
#pour enlever une colonne, par exemple C1, col_names_df_to_remove<-c('C1')
#pour enlever plusierurs colonnes, par exemple C7 et C8, col_names_df_to_remove<-c('C7','C8')
##############################################
col_names_df_to_remove<-c()  ####### C'est là pour enlever des colonnes
###############################################
ProfilByEleve_CAndE_2<-ProfilByEleve_CAndE_2[,!(names(ProfilByEleve_CAndE_2) %in% col_names_df_to_remove)]

# Possibilité d'enlever des lignes sur le même principe que les colonnes
row_names_df_to_remove<-c()
ProfilByEleve_CAndE_2<-ProfilByEleve_CAndE_2[!(row.names(ProfilByEleve_CAndE_2) %in% row_names_df_to_remove),]


print(ProfilByEleve_CAndE_2)

BorneMinVariSup <- ncol(ProfilByEleve_CAndE_2)-1
BorneMaxVariSup <- ncol(ProfilByEleve_CAndE_2)
ca <- CA(ProfilByEleve_CAndE_2, col.sup=c(BorneMinVariSup: BorneMaxVariSup))

explor(ca)
