list.of.packages <- c("explor", "shiny","FactoMineR","tidyverse")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)>0) install.packages(new.packages)

library(explor)
library(FactoMineR)
library(tidyverse)

this.dir <- dirname(rstudioapi::getActiveDocumentContext()$path)
setwd(this.dir)

#ProfilByClasse_CAndE <- read_csv("R/lililot2/Result/Profil/ProfilByClasse_CAndE.csv", col_names = TRUE)
ProfilByClasse_CAndE <- read_csv("../Result/Profil/ProfilByClasse_CAndE.csv", col_names = TRUE)


# rename column 1
colnames(ProfilByClasse_CAndE)[1] <- "new_col"
#set specific column as row names
ProfilByClasse_CAndE_2 <- ProfilByClasse_CAndE %>% column_to_rownames(var="new_col")


ProfilByClasse_CAndE_2 <- subset(ProfilByClasse_CAndE_2, select = c('E1', 'E2', 'E3', 'E4', 'E5', 'E6', 'E7', 'E8', 'E9', 'E10', 'E11', 'Poesie', 'Lycee'))

#Colonne a enlever sous la forme d'un vecteur
#pour ne rien enlever, col_names_df_to_remove<-c()
#pour enlever une colonne, par exemple E1, col_names_df_to_remove<-c('E1')
#pour enlever plusierurs colonnes, par exemple E1 et E3, col_names_df_to_remove<-c('E1','E3')
##############################################
col_names_df_to_remove<-c()  ####### C'est là pour enlever des colonnes
###############################################
ProfilByClasse_CAndE_2<-ProfilByClasse_CAndE_2[,!(names(ProfilByClasse_CAndE_2) %in% col_names_df_to_remove)]

# Possibilité d'enlever des lignes sur le même principe que les colonnes
row_names_df_to_remove<-c()
ProfilByClasse_CAndE_2<-ProfilByClasse_CAndE_2[!(row.names(ProfilByClasse_CAndE_2) %in% row_names_df_to_remove),]

print(ProfilByClasse_CAndE_2)

BorneMinVariSup <- ncol(ProfilByClasse_CAndE_2)-1
BorneMaxVariSup <- ncol(ProfilByClasse_CAndE_2)
ca <- CA(ProfilByClasse_CAndE_2, col.sup=c(BorneMinVariSup: BorneMaxVariSup))

explor(ca)
