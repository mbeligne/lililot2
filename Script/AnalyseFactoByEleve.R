list.of.packages <- c("explor", "shiny","FactoMineR","tidyverse")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)>0) install.packages(new.packages)

library(explor)
library(FactoMineR)
library(tidyverse)

#ProfilByEleve_CAndE <- read_csv("R/lililot2/Result/Profil/ProfilByEleve_CAndE.csv", col_names = TRUE)
ProfilByEleve_CAndE <- read_csv("../Result/Profil/ProfilByEleve_CAndE.csv", col_names = TRUE)


# rename column 1
colnames(ProfilByEleve_CAndE)[1] <- "new_col"
#set specific column as row names
ProfilByEleve_CAndE_2 <- ProfilByEleve_CAndE %>% column_to_rownames(var="new_col")

print(ProfilByEleve_CAndE_2)

pca <- PCA(ProfilByEleve_CAndE_2, scale.unit=TRUE, quanti.sup=c(1: 2))
explor(pca)