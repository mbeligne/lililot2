list.of.packages <- c("explor", "shiny","FactoMineR","readr")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)>0) install.packages(new.packages)

library(explor)
library(FactoMineR)
library(readr)

ProfilByClasse_CAndE <- read_csv("R/Lililot/Result/Profil/ProfilByClasse_CAndE.csv", col_names = TRUE)
ProfilByClasse_CAndE[, c(2:23)] <- sapply(ProfilByClasse_CAndE[, c(2:23)], as.numeric)
#ProfilByClasse_CAndE[,2:23] <- as.data.frame(sapply(ProfilByClasse_CAndE[,2:23], as.character))
#ProfilByClasse_CAndE[,2:23] <- as.data.frame(sapply(ProfilByClasse_CAndE[,2:23], as.numeric))
pca <- PCA(ProfilByClasse_CAndE)
explor(pca)